java -Dspring.profiles.active=local \
     -agentlib:jdwp=transport=dt_socket,server=y,address=8086,suspend=n \
     -Dserver.servlet.context-path=/arbormetrix-xml-converter-service \
     -jar service/target/arbormetrix-xml-converter-*.jar \
     -log4j.configurationFile=classpath:log4j2.xml \
     -port 8080 \
     -start