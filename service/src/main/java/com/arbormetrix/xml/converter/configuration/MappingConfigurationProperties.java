package com.arbormetrix.xml.converter.configuration;

import com.arbormetrix.xml.converter.MappingDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * Loaded from application yml
 */
@Getter
@Setter
@ToString
@Slf4j
@ConfigurationProperties(prefix = "arbormetrix")
public class MappingConfigurationProperties {

    private Map<String, MappingDescription> mappings;

    @PostConstruct
    public void init() {
        LOGGER.info("Configuration properties constructed {}", this);
    }
}
