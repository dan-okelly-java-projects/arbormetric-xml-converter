package com.arbormetrix.xml.converter.configuration;

import com.arbormetrix.xml.converter.MappingDescription;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * On application startup, populate HZ map with mapping entries
 */
@Slf4j
@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(MappingConfigurationProperties.class)
public class HazelcastConfiguration {

    private final HazelcastInstance hazelcastInstance;
    private final MappingConfigurationProperties properties;

    @Bean
    public IMap<String, MappingDescription> mappingDetails() {
        IMap<String, MappingDescription> mappingDetails = hazelcastInstance.getMap("mappingDetails");

        try {
            properties.getMappings().forEach((key, value) ->
                    // Set is more performant then put.  You avoid the cost of serialization.  Same with delete vs remove
                    mappingDetails.set(key, value));
        } catch (Exception e) {
            LOGGER.error("Unable to load patient mappings", e);
        }

        return mappingDetails;
    }
}
