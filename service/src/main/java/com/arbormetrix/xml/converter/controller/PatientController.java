package com.arbormetrix.xml.converter.controller;

import com.arbormetrix.xml.converter.patient.PatientService;
import com.arbormetrix.xml.converter.dto.FileDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * API to consume XML as a string.  Validation happens downstream.  For now exceptions will be logged
 * A better solution would be to bubble up a custom exception and catch with link @{@link org.springframework.web.bind.annotation.ControllerAdvice}
 * and send the appropriate error to the client.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
public class PatientController {

    private final PatientService patientService;

    @PostMapping(produces = APPLICATION_JSON_VALUE, value = "/convert")
    public String convert(@RequestBody FileDto fileDto) {
        return patientService.convert(fileDto.getFileName()); // Normally I use a mapper an map back to a DTO but the test requirements call to not explicitly use any field names
    }
}
