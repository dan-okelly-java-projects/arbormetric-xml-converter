package com.arbormetrix.xml.converter.controller;

import com.arbormetrix.xml.converter.patient.PatientService;
import com.arbormetrix.xml.converter.dto.FileDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
public class PatientControllerTest {

    private FileDto fileDto = new FileDto();

    @Mock
    private PatientService patientService;

    @InjectMocks
    private PatientController patientController;

    @Test
    @DisplayName("When I call the api, I pass in a fileDto object to the service layer and verify convert was called")
    void when_I_call_convert_pass_payload_to_service() {

        patientController.convert(fileDto);

        verify(patientService).convert(fileDto.getFileName());
    }
}