package com.arbormetrix.xml.converter;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Hazelcast Domain objects need to implement Serializable.  A better solution is to use the sub-zero dependency
 * for serialization and override java serialization when it comes to hazelcast objects
 */
@Getter
@Setter
@ToString
public class MappingDescription implements Serializable {

    private String name;
    private String datatype;
    private Map<String, String> map = new HashMap<>();
}
