package com.arbormetrix.xml.converter.patient;

import com.arbormetrix.xml.converter.MappingDescription;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.IMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.util.Iterator;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PatientServiceTest {

    private static final String FILE_NAME = "happy-path-patient-conversion.xml";

    @Mock
    private IMap<String, MappingDescription> mappingDetails;

    @Mock
    private PatientHandler patientHandler;

    private ObjectMapper objectMapper;
    private PatientService patientService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        objectMapper = new ObjectMapper();
        patientService = new PatientService(objectMapper, patientHandler, mappingDetails);
    }

    @DisplayName("load file on the class path, call convert, deserialize the file, check in cache if key mappings exist, pass off the handler if exists, do no process the entry if no mapping")
    @Test
    public void convert_file_process_entries() {

        when(mappingDetails.get(anyString())).thenReturn(any(MappingDescription.class));

        String response = patientService.convert(FILE_NAME);

        assertThat(response).contains("id");
        assertThat(response).contains("gender");
        assertThat(response).contains("state");
        assertThat(response).contains("dateOfBirth");
    }

    @DisplayName("load file on the class path, if File can't be found, throw exception")
    @Test
    public void convert_file_process_entries_file_not_found() {

        Exception exception = assertThrows(
                RuntimeException.class,
                () -> patientService.convert("BAD FILE PATH"));

        assertTrue(exception.getMessage().contains("Unable to find file"));
    }

    //TODO: Tests for logs
}