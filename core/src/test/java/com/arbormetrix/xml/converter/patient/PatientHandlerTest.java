package com.arbormetrix.xml.converter.patient;

import com.arbormetrix.xml.converter.MappingDescription;
import com.arbormetrix.xml.converter.patient.PatientHandler;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.hazelcast.core.IMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PatientHandlerTest {

    private static final String GENDER = "gender";
    private static final String STATE = "state";
    private static final String AGE = "age";
    private static final String SEX = "sex";
    private static final String DATE_OF_BIRTH = "dateOfBirth";

    private Map<String, JsonNode> entryMap = new LinkedHashMap<>();
    private Map<String, String> map = new HashMap<>();
    private Map.Entry<String, JsonNode> entry;
    private ObjectNode objectNode;
    private List<Object> patients;
    private ObjectMapper objectMapper = new ObjectMapper();
    private MappingDescription mappingDescription;

    @Mock
    private IMap<String, MappingDescription> mappingDetails;

    @InjectMocks
    private PatientHandler patientHandler;

    @BeforeEach
    void setUp() {
        entryMap.clear();
        map.clear();
        patients = new ArrayList<>();
        objectNode = objectMapper.createObjectNode();
        mappingDescription = new MappingDescription();
    }

    //TODO: Create tests to handle parsing exceptions

    @Test
    @DisplayName("When I call update and the field is gender I update the node with the field in the map and remove the old field")
    public void update_node_when_is_gender() throws ParseException {

        objectNode.put(GENDER, "m");
        patients.add(objectNode);

        JsonNode jsonNode = objectMapper.valueToTree(patients.get(0));
        entryMap.put(GENDER, jsonNode);

        map.put("m", "male");
        mappingDescription.setMap(map);
        mappingDescription.setName(SEX);

        entry = jsonNode.fields().next();

        when(mappingDetails.get(GENDER)).thenReturn(mappingDescription);

        patientHandler.update(entry, objectNode);  // Updates the object node

        assertThat(objectNode.has(SEX));
        assertThat(objectNode.get(SEX).asText()).isEqualTo("male");

        assertThat(objectNode.get(GENDER)).isNull();
    }

    @Test
    @DisplayName("When I call update and the field is state I update the node with the field in the map")
    public void update_node_when_is_state() throws ParseException {

        objectNode.put(STATE, "michigan");
        patients.add(objectNode);

        JsonNode jsonNode = objectMapper.valueToTree(patients.get(0));
        entryMap.put(STATE, jsonNode);

        map.put("michigan", "MI");
        mappingDescription.setMap(map);
        mappingDescription.setName(STATE);

        entry = jsonNode.fields().next();

        when(mappingDetails.get(STATE)).thenReturn(mappingDescription);

        patientHandler.update(entry, objectNode);  // Updates the object node

        assertThat(objectNode.has(STATE));
        assertThat(objectNode.get(STATE).asText()).isEqualTo("MI");
    }

    @Test
    @DisplayName("When I call update and the field is dob I update the node with the field in the map")
    public void update_node_when_is_date_of_birth() throws ParseException {

        objectNode.put(DATE_OF_BIRTH, "03/04/1962");
        patients.add(objectNode);

        JsonNode jsonNode = objectMapper.valueToTree(patients.get(0));
        entryMap.put(DATE_OF_BIRTH, jsonNode);

        mappingDescription.setName(AGE);

        when(mappingDetails.get(DATE_OF_BIRTH)).thenReturn(mappingDescription);

        entry = jsonNode.fields().next();

        patientHandler.update(entry, objectNode);  // Updates the object node

        assertThat(objectNode.has(AGE));
        assertThat(objectNode.get(DATE_OF_BIRTH)).isNull();
    }
}