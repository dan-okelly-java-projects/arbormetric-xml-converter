package com.arbormetrix.xml.converter.patient;

import com.arbormetrix.xml.converter.MappingDescription;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.hazelcast.core.IMap;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * Handler class for validation and updating field entries
 */
@Component
@RequiredArgsConstructor
public class PatientHandler {

    private static final String DATE_FORMAT = "MM/dd/yyyy";
    private static final String INT = "int";
    private final IMap<String, MappingDescription> mappingDetails;

    private boolean isFieldGender(Map<String, String> descriptionMap) {

        if (descriptionMap.size() != 0) {
            for (String value : descriptionMap.values()) {
                if (value.length() > 2) {  // The only other map in memory is states.  all state values are 2 letter
                    return true;
                }
            }
        }

        return false;
    }

    private boolean isFieldDateOfBirth(String value) {
        DateFormat sdf = new SimpleDateFormat(DATE_FORMAT);  // This would not be acceptable as is.  There are more formats.
        sdf.setLenient(false);

        try {
            sdf.parse(value);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }


    private boolean isFieldState(Map<String, String> descriptionMap) {

        if (descriptionMap.size() != 0) {
            for (String value : descriptionMap.values()) {
                if (value.length() == 2) {  // all state acronyms are two letters
                    return true;
                }
            }
        }

        return false;
    }

    private Map<String, String> getDescriptionMap(String key) {
        return mappingDetails.get(key).getMap();
    }

    private void updateNode(ObjectNode objectNode, String key, String value) {
        objectNode.remove(key); // Remove old data

        MappingDescription mappingDescription = mappingDetails.get(key);

        if (INT.equalsIgnoreCase(mappingDescription.getDatatype())) { // constant .equalsIgnoreCase to avoid NPE
            objectNode.put(mappingDescription.getName(), Integer.valueOf(value));
            return;
        }

        objectNode.put(mappingDescription.getName(), value);
    }

    protected void update(Map.Entry<String, JsonNode> entry, ObjectNode objectNode) throws ParseException {

        String key = entry.getKey();
        Map<String, String> descriptionMap = getDescriptionMap(key);
        String value = entry.getValue().asText().toLowerCase();

        if (isFieldGender(descriptionMap)) {
            updateNode(objectNode, key, descriptionMap.get(value));
            return;
        }

        if (isFieldState(descriptionMap)) {
            updateNode(objectNode, key, descriptionMap.get(value));
            return;
        }

        if (isFieldDateOfBirth(value)) {
            updateNode(objectNode, key, String.valueOf(Period.between(getLocalDate(value), LocalDate.now()).getYears()));
            return;
        }

        updateNode(objectNode, key, entry.getValue().asText());
    }

    private LocalDate getLocalDate(String value) throws ParseException {
        DateFormat format = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
        Date date = format.parse(value);

        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

}
