package com.arbormetrix.xml.converter.patient;

import com.arbormetrix.xml.converter.MappingDescription;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.hazelcast.core.IMap;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Service for handling core business logic needed to convert/lookup xml doc. If the test required transformation of
 * an employee/doctor/etc I would have most likely created a reusable interface.  IMO you should only use interfaces when you can expect more
 * then one implementation or if you have an API that has security/swagger annotations and want to hide the implementation details
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class PatientService {

    private static final XmlMapper xmlMapper = new XmlMapper();

    private final ObjectMapper objectMapper;
    private final PatientHandler patientHandler;
    private final IMap<String, MappingDescription> mappingDetails;

    public String convert(String fileName) {

        try {
            List<Object> patients = loadFile("/".concat(fileName));
            return objectMapper.writeValueAsString(toJson(patients));
        } catch (IOException e) {
            throw new RuntimeException("Unable to load file", e);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Unable to find file", e);
        } catch (Exception e) {
            throw new RuntimeException("Unable to process request", e);  //TODO: Create custom exception
        }
    }

    private List<Object> loadFile(String fileName) throws IOException {
        return xmlMapper.readValue(this.getClass().getResourceAsStream(fileName), List.class);
    }

    private List<ObjectNode> toJson(List<Object> patients) {

        List<ObjectNode> objectNodes = new ArrayList<>();

        patients.forEach(obj -> {
            JsonNode jsonNode = objectMapper.valueToTree(obj);

            Iterator<Map.Entry<String, JsonNode>> fields = jsonNode.fields();
            ObjectNode childNode = jsonNode.deepCopy();

            while (fields.hasNext()) {
                handle(fields.next(), childNode);
            }

            objectNodes.add(childNode);
        });

        return objectNodes;
    }

    private void handle(Map.Entry<String, JsonNode> entry, ObjectNode objectNode) {

        String key = entry.getKey();

        if (mappingDetails.get(key) == null) {
            LOGGER.debug("There is not a mapping for this key {}", key);
            return;
        }

        try {
            patientHandler.update(entry, objectNode);
        } catch (ParseException e) {
            LOGGER.warn("unable to parse entry", e);
        }
    }
}
