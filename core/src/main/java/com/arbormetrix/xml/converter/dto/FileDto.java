package com.arbormetrix.xml.converter.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FileDto {
    private String fileName = "happy-path-patient-conversion.xml";  // Instantiate it here so we never need to worry about it being null later
}
