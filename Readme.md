The arbormetrix-xml-converter-service is a small maven multi module spring boot service.  The service has one API and uses hazelcast
to cache mapping field descriptions.

on application startup, mappings from the application yml are loaded into a hz map.  This map is later injected 
into service components.

When the convert API is invoked, the service will look for the fileName given in the request.  If successful, the service will
deserialize the xml into a List<Object> we iterate through the objects and transform them into JsonNodes.  

The key from the nodes are then inspected to see if we have a mapping value for them.  If we do, we pass the key and value
off to the handler to try and determine what we should do with the data.

after all nodes have been iterated through and updated, return the collection of ObjectNodes back and use jackson to Serialize
back to the API as a string representation of Json.

***Start up requirements***

In order to start this service you will need the following:

jdk 12 or greater (brew cask install adoptopenjdk12)
maven
postman or terminal

--
Build the service and compile it in jdk 12 or above.  This can be done through the command line if you have maven installed:  mvn -U clean install

Run the service with the start.sh script.
make sure port 8080, port 8086 (debug port), and port 8085(hazelcast port) are not being used.

When the service is up and running. Import the postman collection found in the root directory.  -- XML-Converter.postman_collection.json

to call the API - either use the postman collection or the curl statement below

curl --location --request POST 'http://localhost:8080/arbormetrix-xml-converter-service/convert' \
--header 'Content-Type: application/json' \
--data-raw '{
    "fileName": "happy-path-patient-conversion.xml"
}'

***Custom Files***

If you would like to use your own XML file you'll need to follow these steps:

1. Upload the file to src/main/resources directory.  This file should live in the same path as happy-path-patient-conversion.xml
2. Change API body or curl fieldName property to new file name